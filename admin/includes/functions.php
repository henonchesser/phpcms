<?php
    
    function confirmQuery ($query){

        global $conn;

        if($query){
            
            //worked fine
        
        } else {
            die( "Something Went Wrong" . mysqli_error($conn));
        }
    }

    function handle_category() {
        global $conn;
        //create categories
        if(isset($_POST['submit'])) {
            $cat_title = $_POST['cat_title'];

            if($cat_title == "" || empty($cat_title)){

                echo "You must enter a value";

            } else {
                $query  = "INSERT INTO categories(cat_title) ";
                $query .= "VALUES('{$cat_title}')";

                $add_category_query = mysqli_query($conn, $query);

                if ( !$add_category_query ){
                    die('Add Item Failed' . mysqli_error);
                } else {
                    echo "Added {$cat_title}";
                }

            }

        }

         //update categories
         if(isset($_POST['update'])) {
             $cat_title = $_POST['cat_title'];
             $cat_id = $_POST['cat_id'];

             if($cat_title == "" || empty($cat_title)){

                 echo "You must enter a value";

             } else {
                 $query  = "UPDATE categories SET cat_title = '{$cat_title}' ";
                 $query .= "WHERE cat_id = {$cat_id}";

                 $update_category_query = mysqli_query($conn, $query);

                 if ( !$update_category_query ){
                     die('Add Update Failed' . mysqli_error);
                 } else {
                     echo "Updated {$cat_title}";
                     header("Location: categories.php");
                 }

             }
         }

        //drop categories
        if (isset($_GET['delete'])){
            $this_cat_id = $_GET['delete'];

            $query = "DELETE FROM categories WHERE cat_id = {$this_cat_id}";

            $delete_cat_query = mysqli_query($conn, $query);

            header("Location: categories.php");
        }
    };

?>