<?php

    if(isset($_POST['edit_post'])){

        $post_id = $_POST["post_id"];
        $post_title = $_POST["title"];
        $post_cat_id = $_POST["post_cat_id"];
        $post_author = $_POST["post_author"];
        $post_status = $_POST["post_status"];

        $post_image = $_FILES['post_image']['name'];
        $post_image_temp = $_FILES['post_image']['tmp_name'];

        $post_tags = $_POST["post_tags"];
        $post_content = $_POST["post_content"];
        $post_date = date('d-m-y');
        $post_comment_count = 4;

        //move image from temp location to images file
        move_uploaded_file($post_image_temp, "../images/$post_image");

        

        //insert post
        $query = "UPDATE posts SET";
        $query .= " post_title = '{$post_title}'";
        $query .= " ,post_author = '{$post_author}'";
        $query .= " ,post_date = now()";
        if(!empty($post_image)){
            $query .= " ,post_image = '{$post_image}'";
        }
        $query .= " ,post_content = '{$post_content}'";
        $query .= " ,post_tags ='{$post_tags}'";
        $query .= " ,post_comment_count = '{$post_comment_count}'";
        $query .= " ,post_status = '{$post_status}'";
        $query .= "WHERE post_id = {$post_id}";

        $insert_new_post = mysqli_query($conn, $query);

        confirmQuery($insert_new_post);
    }


    if(isset($_GET['id'])){
        
        $this_post_id = $_GET['id'];
        
        $query = "SELECT * FROM posts WHERE post_id = {$this_post_id}";
        $select_posts = mysqli_query($conn, $query);

        $row = mysqli_fetch_assoc($select_posts);
    
        $post_id = $row['post_id'];
        $post_author = $row['post_author'];
        $post_title = $row['post_title'];
        $post_cat_id = $row['post_cat_id'];
        $post_status = $row['post_status'];
        $post_image = $row['post_image'];
        $post_content = $row['post_content'];
        $post_tags = $row['post_tags'];
        $post_comment_count = $row['post_comment_count'];
        $post_date = $row['post_date'];

    }

    


?>


<form action="" method="post" enctype="multipart/form-data">
    <div class="form-group">
        <label for="title">Post Title</label>
        <input type="text" name="title" id="title" class="form-control" value="<?php echo $post_title ?>">
    </div>

    <div class="form-group">
        <label for="post_cat_id">Post Category Id</label>
        <select name="post_cat_id" id="post_cat_id" class="form-control">
        <?php 
                //read categories
            $query = "SELECT * FROM categories";
            $select_cat_sidebar = mysqli_query($conn, $query);

            confirmQuery($select_cat_sidebar);

            //display caterogies
            while ( $row = mysqli_fetch_assoc($select_cat_sidebar)){
                $cat_id = $row['cat_id'];
                $cat_title = $row['cat_title'];

                $isSelected = "";

                if ($post_cat_id == $cat_id){
                    $isSelected = "selected";
                }

                echo "<option value='{$cat_id}' {$isSelected}>{$cat_title}</option>";
            }
        ?>
        </select>


    </div>

    <div class="form-group">
        <label for="post_author">Post Author</label>
        <input type="text" name="post_author" id="post_author" class="form-control" value="<?php echo $post_author ?>">
    </div>

    <div class="form-group">
        <label for="post_status">Post Status</label>
        <input type="text" name="post_status" id="post_status" class="form-control" value="<?php echo $post_status ?>">
    </div>

    <div class="form-group">
        <label for="post_image">Post Image</label>
        <img width="100" src="../images/<?php echo $post_image?>" alt="">
        <input type="file" name="post_image" id="post_image" >
    </div>

    <div class="form-group">
        <label for="post_content">Post Content</label>
        <textarea type="file" name="post_content" id="post_content" class="form-control" cols="30" rows="10" ><?php echo $post_content ?></textarea>
    </div>

    <div class="form-group">
        <label for="post_tags">Post Tags</label>
        <input type="text" name="post_tags" id="post_tags" class="form-control" value="<?php echo $post_tags ?>">
    </div>

    <div class="form-group">
        <input type="hidden" name="post_id" value="<?php echo $post_id ?>">
        <input class="btn btn-primary" type="submit" name="edit_post" value="Edit Post">
    </div>

</form>