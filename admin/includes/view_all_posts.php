<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>ID</th>
            <th>Author</th>
            <th>Title</th>
            <th>Caegory</th>
            <th>Status</th>
            <th>Image</th>
            <th>Tags</th>
            <th>Comments</th>
            <th>Date</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
    </thead>
    <tbody>

        <?php 
            //read posts
            $query = "SELECT * FROM posts";
            $select_posts = mysqli_query($conn, $query);

            //display posts
            while ( $row = mysqli_fetch_assoc($select_posts)){
                $post_id = $row['post_id'];
                $post_author = $row['post_author'];
                $post_title = $row['post_title'];
                $post_cat_id = $row['post_cat_id'];
                $post_status = $row['post_status'];
                $post_image = $row['post_image'];
                $post_tags = $row['post_tags'];
                $post_comment_count = $row['post_comment_count'];
                $post_date = $row['post_date'];

                
            echo "<tr>";
                echo "<td>$post_id</td>";
                echo "<td>$post_author</td>";
                echo "<td>$post_title</td>";

                $query = "SELECT * FROM categories WHERE cat_id = {$post_cat_id}";
                $select_cat_name = mysqli_query($conn, $query);
                $row = mysqli_fetch_assoc($select_cat_name);
                $this_cat_name = $row['cat_title'];
    

                echo "<td>$this_cat_name</td>";

                echo "<td>$post_status</td>";
                echo "<td><img width='100' src='../images/$post_image' ></td>";
                echo "<td>$post_tags</td>";
                echo "<td>$post_comment_count</td>";
                echo "<td>$post_date</td>";
                echo "<td><a href='posts.php?source=edit_post&id={$post_id}'>Edit</a></td>";
                echo "<td><a href='posts.php?delete={$post_id}'>Delete</a></td>";
                
            echo "</tr>";
                

            }      
            
            
            if(isset($_GET['delete'])){
                $post_id =  $_GET['delete'] ;

                $query = "DELETE FROM posts where post_id = {$post_id}";

                $delete_post = mysqli_query($conn, $query);
                header("Location: posts.php");
            }
        ?>

            

        
        
    </tbody>
</table>