<?php
    if(isset($_POST['create_post'])){

        $post_title = $_POST["title"];
        $post_cat_id = $_POST["post_cat_id"];
        $post_author = $_POST["post_author"];
        $post_status = $_POST["post_status"];

        $post_image = $_FILES['post_image']['name'];
        $post_image_temp = $_FILES['post_image']['tmp_name'];

        $post_tags = $_POST["post_tags"];
        $post_content = $_POST["post_content"];
        $post_date = date('d-m-y');
        //$post_comment_count = 4;

        //move image from temp location to images file
        move_uploaded_file($post_image_temp, "../images/$post_image");

        //insert post
        $query = "INSERT INTO posts (post_cat_id, post_title, post_author, post_date, post_image, post_content,
                     post_tags, post_comment_count, post_status)";
        $query .= " VALUES ({$post_cat_id}, '{$post_title}', '{$post_author}', now(), '{$post_image}', '{$post_content}',
             '{$post_tags}', 0, '{$post_status}')";

        $insert_new_post = mysqli_query($conn, $query);

       confirmQuery($insert_new_post);
    }
?>

<form action="" method="post" enctype="multipart/form-data">
    <div class="form-group">
        <label for="title">Post Title</label>
        <input type="text" name="title" id="title" class="form-control">
    </div>

    <div class="form-group">
        <label for="post_cat_id">Post Category Id</label>
        <select name="post_cat_id" id="post_cat_id" class="form-control">
        <option value="">Select a Category</option>
        <?php 
                //read categories
            $query = "SELECT * FROM categories";
            $select_cat = mysqli_query($conn, $query);

            confirmQuery($select_cat);

            //display caterogies
            while ( $row = mysqli_fetch_assoc($select_cat)){
                $cat_id = $row['cat_id'];
                $cat_title = $row['cat_title'];

                echo "<option value='{$cat_id}' >{$cat_title}</option>";
            }
        ?>
        </select>
    </div>

    <div class="form-group">
        <label for="post_author">Post Author</label>
        <input type="text" name="post_author" id="post_author" class="form-control">
    </div>

    <div class="form-group">
        <label for="post_status">Post Status</label>
        <input type="text" name="post_status" id="post_status" class="form-control">
    </div>

    <div class="form-group">
        <label for="post_image">Post Image</label>
        <input type="file" name="post_image" id="post_image" >
    </div>

    <div class="form-group">
        <label for="post_content">Post Content</label>
        <textarea type="file" name="post_content" id="post_content" class="form-control" cols="30" rows="10"></textarea>
    </div>

    <div class="form-group">
        <label for="post_tags">Post Tags</label>
        <input type="text" name="post_tags" id="post_tags" class="form-control">
    </div>

    <div class="form-group">
        <input class="btn btn-primary" type="submit" name="create_post" value="Publish Post">
    </div>

</form>