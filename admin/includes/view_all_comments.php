<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>ID</th>
            <th>Author</th>
            <th>Email</th>
            <th>Comment</th>
            <th>In Response to</th>
            <th>Status</th>
            <th>Date</th>
            <th>Approve</th>
            <th>Unapprove</th>
            <th>Delete</th>
        </tr>
    </thead>
    <tbody>

        <?php 
            //read categories
            $query = "SELECT * FROM comments";
            $select_posts = mysqli_query($conn, $query);

            //display caterogies
            while ( $row = mysqli_fetch_assoc($select_posts)){
                $comment_id = $row['comment_id'];
                $comment_author = $row['comment_author'];
                $comment_post_id = $row['comment_post_id'];
                $comment_email = $row['comment_email'];
                $comment_content = substr($row['comment_content'],0,100);
                $comment_status = $row['comment_status'];
                $comment_date = $row['comment_date'];


                
            echo "<tr>";
                echo "<td>$comment_id</td>";
                echo "<td>$comment_author</td>";
                echo "<td>$comment_email</td>";
                echo "<td>$comment_content</td>";

                $query = "SELECT * FROM posts WHERE post_id = {$comment_post_id}";
                $select_post_id_query = mysqli_query($conn, $query);
                $row = mysqli_fetch_assoc($select_post_id_query);
                $post_title = $row['post_title'];

                echo "<td><a href='../post.php?id={$comment_post_id}'>$post_title</a></td>";
                

                echo "<td>$comment_status</td>";
                echo "<td>$comment_date</td>";
                echo "<td><a href='comments.php?approve={$comment_id}'>Approve</a></td>";
                echo "<td><a href='comments.php?unapprove={$comment_id}'>Unapprove</a></td>";
                echo "<td><a href='comments.php?delete={$comment_id}'>Delete</a></td>";
                
            echo "</tr>";
                

            }      
            
            
            if(isset($_GET['delete'])){
                $comment_id =  $_GET['delete'] ;

                $query = "DELETE FROM comments where comment_id = {$comment_id}";

                $delete_comment = mysqli_query($conn, $query);
                header("Location: comments.php");
            }


            if(isset($_GET['unapprove'])){
                $comment_id =  $_GET['unapprove'] ;

                $query = "UPDATE comments SET comment_status = 'unapproved' where comment_id = {$comment_id}";

                $unapprove_comment = mysqli_query($conn, $query);
                header("Location: comments.php");
            }

            if(isset($_GET['approve'])){
                $comment_id =  $_GET['approve'] ;

                $query = "UPDATE comments SET comment_status = 'approved' where comment_id = {$comment_id}";

                $approve_comment = mysqli_query($conn, $query);
                header("Location: comments.php");
            }
        ?>

            

        
        
    </tbody>
</table>