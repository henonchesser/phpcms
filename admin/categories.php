
<?php include "includes/admin_header.php"?>
<div id="wrapper">
   <?php include "includes/admin_nav.php"?>

    <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        Categories
                        <small>Subheading</small>
                    </h1>

                    <div class="col-xs-6">

                        <form action="" method="post">
                            
                            <div class="form-group">
                                <label for="cat_title">Add Category</label>
                                <input type="text" class="form-control" name="cat_title">
                            </div>

                            <div class="form-group">
                                <input class="btn btn-primary" type="submit" name="submit" value="Add">
                            </div>

                        </form>

                        
                            
                        <?php 
                            if (isset($_GET["update"])){
                                $thisUpdateId = $_GET["update"];

                                $query = "SELECT cat_id, cat_title FROM categories WHERE cat_id = {$thisUpdateId}";

                                $get_cat_to_update = mysqli_query($conn, $query);
                                

                                if ($get_cat_to_update){
                                    $results =  $catIdToUpdate = mysqli_fetch_assoc($get_cat_to_update);

                                    $thisUpdateId = $results["cat_id"];
                                    $thisUpdateTitle = $results["cat_title"];

                        ?>

                                    <form action="" method="post">  

                                    
                                        <div class="form-group">
                                            <label for="cat_title">Edit Category</label>
                                            <input type="text" class="form-control" name="cat_title" value="<?php echo $thisUpdateTitle?>">
                                        </div>

                                        <div class="form-group">
                                            <input type="hidden" name="cat_id" value="<?php echo $thisUpdateId?>">
                                            <input class="btn btn-primary" type="submit" name="update" value="Update">
                                        </div>

                                    </form>

                        <?php
                                    
                                } else {
                                    echo "No item with ID: " . $thisUpdateId;
                                }

                            }
                        ?>


                    </div>

                    <?php
                         handle_category();


                    ?>

                    <div class="col-xs-6">
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Cat Title</th>
                                    <th>Update</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                        //read categories
                                    $query = "SELECT * FROM categories";
                                    $select_cat_sidebar = mysqli_query($conn, $query);

                                    //display caterogies
                                    while ( $row = mysqli_fetch_assoc($select_cat_sidebar)){
                                        $cat_id = $row['cat_id'];
                                        $cat_title = $row['cat_title'];
                                ?>

                                <tr>
                                    <td><?php echo $cat_id?></td>
                                    <td><?php echo $cat_title?></td>
                                    <td><a href="categories.php?update=<?php echo $cat_id ?>">Update</a></td>
                                    <td><a href="categories.php?delete=<?php echo $cat_id ?>">Delete</a></td>
                                </tr>

                                <?php } ?>
                            </tbody>
                        </table>

                    </div>


                    <!-- <ol class="breadcrumb">
                        <li>
                            <i class="fa fa-dashboard"></i>  <a href="index.html">Dashboard</a>
                        </li>
                        <li class="active">
                            <i class="fa fa-file"></i> Categories
                        </li>
                    </ol> -->
                </div>
            </div>
            <!-- /.row -->

        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<?php include "includes/admin_footer.php" ?>