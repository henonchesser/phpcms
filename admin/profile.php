
<?php include "includes/admin_header.php"?>
<div id="wrapper">
   <?php include "includes/admin_nav.php"?>

   <?php
        
        if(isset($_POST['edit_user'])){

            $user_id = $_POST['user_id'];
            $username = $_POST["username"];
            $user_firstname = $_POST["user_firstname"];
            $user_lastname = $_POST["user_lastname"];
            $user_email = $_POST["user_email"];
            $user_password = $_POST["user_password"];
            $user_role = $_POST["user_role"];
    
            
    
            //insert post
            $query = "UPDATE users SET";
            $query .= " username = '{$username}'";
            $query .= " ,user_firstname = '{$user_firstname}'";
            $query .= " ,user_lastname = '{$user_lastname}'";
            $query .= " ,user_email = '{$user_email}'";
            if ($user_password != ''){
                $query .= " ,user_password = '{$user_password}'";
            }
            
            $query .= " ,user_role = '{$user_role}'";
            $query .= "WHERE user_id = {$user_id}";
    
            $insert_new_post = mysqli_query($conn, $query);
    
            confirmQuery($insert_new_post);
        }
    
        
        if(isset($_SESSION["username"])){
        
            $this_username = $_SESSION["username"];
            
            $query = "SELECT * FROM users WHERE username = '{$this_username}'";
            $select_posts = mysqli_query($conn, $query);
    
            $row = mysqli_fetch_assoc($select_posts);
        
            $user_id = $row['user_id'];
            $username = $row["username"];
            $user_firstname = $row["user_firstname"];
            $user_lastname = $row["user_lastname"];
            $user_email = $row["user_email"];
            $user_password = $row["user_password"];
            $user_role = $row["user_role"];
        };
   ?>

    <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        Users
                        <!-- <small>Subheading</small> -->
                    </h1>

                    <form action="" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                                <label for="username">Username</label>
                                <input type="text" name="username" id="username" class="form-control" value="<?php echo $username ?>">
                            </div>

                            <div class="form-group">
                                <label for="user_firstname">First Name</label>
                                <input type="text" name="user_firstname" id="user_firstname" class="form-control" value="<?php echo $user_firstname ?>">
                            </div>

                            <div class="form-group">
                                <label for="user_lastname">Last Name</label>
                                <input type="text" name="user_lastname" id="user_lastname" class="form-control" value="<?php echo $user_lastname ?>">
                            </div>

                            <div class="form-group">
                                <label for="user_email">Email</label>
                                <input type="email" name="user_email" id="user_email" class="form-control" value="<?php echo $user_email ?>">
                            </div>

                            <div class="form-group">
                                <label for="post_tags">Password</label>
                                <input type="password" name="user_password" id="user_password" class="form-control" >
                            </div>

                            <div class="form-group">
                                <label for="user_role">User Role</label>
                                <select name="user_role" id="user_role" class="form-control">
                                <option value="Subscriber">Select Option</option>
                                <option value="Admin">Admin</option>
                                <option value="Subscriber">Subscriber</option>
                                    
                                </select>
                            
                            </div>

                            <div class="form-group">
                                <input type="hidden" value="<?php echo $user_id?>" name="user_id" id="user_id">
                                <input class="btn btn-primary" type="submit" name="edit_user" value="Edit User">
                            </div>

                        </form>
                </div>
            </div>
            <!-- /.row -->

        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<?php include "includes/admin_footer.php" ?>