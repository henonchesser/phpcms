<?php include "includes/db.php"; ?>
<?php include "includes/header.php"; ?>
<?php include "includes/nav.php" ; ?>


        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-8">

            <h1 class="page-header">
                Page Heading
                <small>Secondary Text</small>
            </h1>

                <?php

                    if(isset($_GET['id'])){
                        $this_post_id = $_GET['id'];
                    } else {
                        $this_post_id = 0;
                    }

                $query = "SELECT * FROM posts WHERE post_id = {$this_post_id}";
                    $query_get_all_posts = mysqli_query($conn, $query);

                    while ( $row = mysqli_fetch_assoc($query_get_all_posts)){
                        
                        $post_title = $row['post_title'];
                        $post_id = $row['post_id'];
                        $post_author = $row['post_author'];
                        $post_date = $row['post_date'];
                        $post_image = $row['post_image'];
                        $post_content = $row['post_content'];
                ?>

                    <!-- First Blog Post -->
                    <h2>
                        <a href="#"><?php echo $post_title ?></a>
                    </h2>
                    <p class="lead">
                        by <a href="index.php"><?php echo $post_author ?></a>
                    </p>
                    <p><span class="glyphicon glyphicon-time"></span> Posted on <?php echo $post_date ?></p>
                    <hr>
                    <img class="img-responsive" src="images/<?php echo $post_image ?>" alt="">
                    <hr>
                    <p><?php echo $post_content ?></p>
                    <a class="btn btn-primary" href="#">Read More <span class="glyphicon glyphicon-chevron-right"></span></a>

                    <hr>


                <?php }; ?>

                
                <!-- Blog Comments -->

                <?php
                    if(isset($_POST['create_comment'])){

                       $comment_post_id = $_POST['post_id'];
                       $comment_author = $_POST['comment_author'];
                       $comment_email = $_POST['comment_email'];
                       $comment_content = $_POST['comment_content'];

                       $query = "INSERT INTO comments ";
                       $query .= "(comment_post_id, comment_author, comment_email, comment_content, comment_status, comment_date) ";
                       $query .= "VALUES ";
                       $query .= "({$comment_post_id}, '{$comment_author}', '{$comment_email}', '{$comment_content}', 'unapproved', now()) ";


                       $create_comment_query = mysqli_query($conn, $query);

                       if (!$create_comment_query){
                        echo "Query Error " . mysqli_error($conn);

                       }

                       //increase comment counter
                       $query = "UPDATE posts SET post_comment_count = post_comment_count + 1 WHERE post_id = {$comment_post_id}";
                       $update_comment_count_query = mysqli_query($conn, $query);

                       if (!$update_comment_count_query){
                        echo "Query Error " . mysqli_error($conn);
                       }



                    }

                ?>

                <!-- Comments Form -->
                <div class="well">
                    <h4>Leave a Comment:</h4>
                    <form role="form" action="" method="post">
                        <div class="form-group">
                            <label for="comment_author">Author</label>
                            <input type="text" name="comment_author" id="comment_author" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="comment_email">Email</label>
                            <input type="email" name="comment_email" id="comment_email" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="comment_content">Comment</label>
                            <textarea class="form-control" name="comment_content" rows="3"></textarea>
                        </div>
                        <input type="hidden" name="post_id" value="<?php echo $post_id ?>">
                        <button type="submit" class="btn btn-primary" name="create_comment">Submit</button>
                    </form>
                </div>

                <hr>

                <!-- Posted Comments -->

                <?php
                    $query = "SELECT * FROM comments WHERE comment_post_id = {$post_id} ";
                    $query .= "AND comment_status = 'approved' ";
                    $query .= "ORDER BY comment_id desc";
                    $get_comment_query = mysqli_query($conn, $query);

                    while ($row = mysqli_fetch_assoc($get_comment_query)){
                        $comment_author = $row['comment_author'];
                        $comment_content = $row['comment_content'];
                        $comment_date = $row['comment_date'];
                    
                ?>

                <!-- Comment -->
                <div class="media">
                    <a class="pull-left" href="#">
                        <img class="media-object" src="http://placehold.it/64x64" alt="">
                    </a>
                    <div class="media-body">
                        <h4 class="media-heading"><?php echo $comment_author ?>
                            <small><?php echo $comment_date ?></small>
                        </h4> 
                        <?php echo $comment_content ?>

                    </div>
                </div>

                <?php } ?>


            </div>

        <?php include "includes/sidebar.php" ?>

        </div>
        <!-- /.row -->

        <hr>

<?php include "includes/footer.php"; ?>