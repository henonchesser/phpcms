<?php include "includes/db.php"; ?>
<?php include "includes/header.php"; ?>
<?php include "includes/nav.php" ; ?>

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-8">

            <h1 class="page-header">
                Page Heading
                <small>Secondary Text</small>
            </h1>

                <?php

                    if(isset($_GET['cat'])){
                        $this_post_cat = $_GET['cat'];
                    } else {
                        $this_post_cat = 0;
                    }

                    $query = "SELECT * FROM posts WHERE post_cat_id = {$this_post_cat}";
                    $query_get_all_posts = mysqli_query($conn, $query);

                    while ( $row = mysqli_fetch_assoc($query_get_all_posts)){
                        $post_id = $row['post_id'];
                        $post_title = $row['post_title'];
                        $post_author = $row['post_author'];
                        $post_date = $row['post_date'];
                        $post_image = $row['post_image'];
                        $post_content = substr($row['post_content'], 0, 100);
                ?>

                    <!-- First Blog Post -->
                    <h2>
                        <a href="post.php?id=<?php echo $post_id?>"><?php echo $post_title ?></a>
                    </h2>
                    <p class="lead">
                        by <a href="index.php"><?php echo $post_author ?></a>
                    </p>
                    <p><span class="glyphicon glyphicon-time"></span> Posted on <?php echo $post_date ?></p>
                    <hr>
                    <img class="img-responsive" src="images/<?php echo $post_image ?>" alt="">
                    <hr>
                    <p><?php echo $post_content ?></p>
                    <a class="btn btn-primary" href="#">Read More <span class="glyphicon glyphicon-chevron-right"></span></a>

                    <hr>


                <?php }; ?>

                

            </div>

        <?php include "includes/sidebar.php" ?>

        </div>
        <!-- /.row -->

        <hr>

<?php include "includes/footer.php"; ?>